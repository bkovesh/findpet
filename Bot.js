const vk = require('vk-easy');
var PF = require('./helpers/BotProfile');
var { DbInteraction } = require('./helpers/DbInteraction');
var { BOT_TOKEN } = process.env



var Bot = class Bot {
  constructor() {
    this.name = "FindPets Bot";
    this.NOTEBOOK = {}
    this.PF = PF
  }




  dialog(req, type) {
    let O = req.body.object
    let userId = req.body.object.user_id
    let msgText = req.body.object.body
    let attachments = req.body.object.attachments
    console.log('msgText',req.body.object)
    let QS = this.PF.QUESTIONS


    if (!this.NOTEBOOK[userId])
      this.NOTEBOOK[userId] = {}
    let NB = this.NOTEBOOK[userId]
    if (!NB.questionName) {
      NB.questionName = 'Приветствие'
    }
    let kbVariants = QS['Приветствие'].variants
    let nextQuestion = QS['Приветствие'].question
    let questionName = 'Приветствие'





    let Q = QS[NB.questionName]



    if ( Array.isArray(Q.variants) ) { // если список
      Q.variants.map((v) => {
        if (msgText === v.answer) {
          let nextQ = QS[v.nextQ]
          nextQuestion = nextQ.question
          questionName = v.nextQ
          if ( Array.isArray(nextQ.variants) ) {
            kbVariants = QS[v.nextQ].variants
          } else {
            let userValue = NB[nextQ.variants.if]
            kbVariants = QS[v.nextQ].variants.variants[userValue]
          }
        }
      })
    } else { // если объект
      let userValue = NB[Q.variants.if]
      console.log('userValue', userValue)
      console.log('NB', NB)

      if (userValue) {
        let variants = Q.variants.variants[userValue]
        console.log('variants', variants)
        variants.map((v) => {
          if (msgText === v.answer) {
            let nextQ = QS[v.nextQ]
            nextQuestion = nextQ.question
            questionName = v.nextQ
            if (Array.isArray(nextQ.variants)) {
              kbVariants = QS[v.nextQ].variants
            } else {
              let userValue = NB[nextQ.variants.if]
              kbVariants = QS[v.nextQ].variants.variants[userValue]
            }
          }
        })
      }
    }


    if (O.attachments) {
      let q = 'Фото'
      questionName = q
      nextQuestion = QS[q].question

      if (Array.isArray(QS[q].variants)) {
        kbVariants = QS[q].variants
      } else {
        let userValue = NB[QS[q].variants.if]
        kbVariants = QS[q].variants.variants[userValue]
      }
    }


    if (O.geo) {
      let q = 'Питомец'
      questionName = q
      nextQuestion = QS[q].question

      if (Array.isArray(QS[q].variants)) {
        kbVariants = QS[q].variants
      } else {
        let userValue = NB[QS[q].variants.if]
        kbVariants = QS[q].variants.variants[userValue]
      }
    }


    if (Q.action) {
      if (Q.action.db) {
        if (Q.action.db==='find_images') {
          DbInteraction.findInDb('data')
        }
      }
    }


    console.log('kbVariants',kbVariants)
    let kb = this.setKeyboard(kbVariants)
    this.sendMessage(userId, nextQuestion, kb)
    this.addToResult(userId, NB.questionName, msgText)
    NB.questionName = questionName



  }



  addToResult(userId, key, t) {
    this.NOTEBOOK[userId][key] = t
    console.log('To Notebook', key, t)
  }


  sendMessage(id, text, kb) {
    vk('messages.send', {
      access_token: BOT_TOKEN,
      user_id: id,
      message: text,
      keyboard: JSON.stringify(kb)
    }).then(console.log);
  }


  setKeyboard(arr) {
    let buttons = []

    if ( Array.isArray(arr) ) { // если список
      arr.map((item, i) => {
        if (i===0||(i+1)%3===0) {buttons.push([])} // на каждый 4й добавить
        buttons[buttons.length-1].push({
          "action": {
            "type": "text",
            "payload": "{\"button\": \"" + i + "\"}",
            "label": item.answer
          },
          "color": "default"
        })
      })
    }

    var res = {
      "one_time": false,
      "buttons": buttons
    }
    console.log('keyboard', res)

    return res
  }


}



module.exports = Bot;




/*

this.PF.QUESTIONS.map((Q, i) => {
  let nextQ;
  if (PF.QUESTIONS[i+1]) {
    nextQ = PF.QUESTIONS[i+1]
  } else { return }


  if ( Array.isArray(Q.variants) ) { // если список
    // значение ответа на один из предыдущих вопросов
    let userValue = this.NOTEBOOK[userId][nextQ.variants.if]
    // варианты ответов на следующий вопрос
    let nextVariants = []
    if ( Array.isArray(nextQ.variants) ) {
      nextVariants = nextQ.variants
    } else {
      nextVariants = nextQ.variants.variants[userValue]
    }
    // console.log('userValue',userValue)
    Q.variants.map((v) => {
      if (msgText === v.answer) {
        console.log('ccc')
        kbVariants = nextVariants
        nextQuestion = v.nextQ
        questionName = Q.name
      }
    })
  } else { // если объект
    // значение ответа на один из предыдущих вопросов
    let userValue = this.NOTEBOOK[userId][Q.variants.if]
    // варианты ответов на следующий вопрос
    let variants = Q.variants.variants[userValue]
    // console.log('userValue',userValue)
    if (variants) {
      variants.map((v)=>{
        if (msgText === v) {
          kbVariants = variants
          nextQuestion = v.nextQ
          questionName = Q.name
        }
      })
    }
  }
})

if (!nextQuestion) {
  kbVariants = PF.QUESTIONS[0].variants
  nextQuestion = PF.QUESTIONS[0].question
  questionName = PF.QUESTIONS[0].name
}

console.log('kbVariants',kbVariants)
let kb = this.setKeyboard(kbVariants)
this.sendMessage(userId, nextQuestion, kb)
this.addToResult(userId, questionName, msgText)


}






for (var name in QS) {
  let Q = QS[name]

  if ( Array.isArray(Q.variants) ) { // если список
    Q.variants.map((v) => {
      if (msgText === v.answer) {
        let nextQ = QS[v.nextQ]
        nextQuestion = nextQ.question
        questionName = Q.name
        if ( Array.isArray(nextQ.variants) ) {
          kbVariants = QS[v.nextQ].variants
        } else {
          let userValue = NB[nextQ.variants.if]
          kbVariants = QS[v.nextQ].variants.variants[userValue]
        }
      }
    })
  } else { // если объект
    // значение ответа на один из предыдущих вопросов
    let userValue = NB[Q.variants.if]
    // варианты ответов на следующий вопрос
    let variants = Q.variants.variants[userValue]

    if (variants) {
      variants.map((v) => {
        if (msgText === v.answer) {
          let nextQ = QS[v.nextQ]
          nextQuestion = nextQ.question
          questionName = Q.name
          if ( Array.isArray(nextQ.variants) ) {
            kbVariants = QS[v.nextQ].variants
          } else {
            let userValue = NB[nextQ.variants.if]
            kbVariants = QS[v.nextQ].variants.variants[userValue]
          }
        }
      })
    }
  }
}


*/