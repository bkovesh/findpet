const vk = require('vk-easy');
require('dotenv').load();
const natural = require('natural');
const ndarray = require('ndarray');
const ops = require('ndarray-ops');
const axios = require('axios');


///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////


var DbInteraction = class DbInteraction {
  constructor() {
    this.name = "Database interaction";
    this.DbUrl = 'http://35.228.237.16:5000'
  }



  sendToDb(data) {
    axios.post(this.DbUrl + '/populate/', {
      headers: {
        'Content-Type': 'application/json'
      }
    }, data)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }



  findInDb(data) {
    axios.post(this.DbUrl + '/search_trans/', {
      headers: {
        'Content-Type': 'application/json'
      }
    }, data)
      .then(function (response) {
        console.log(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }


}


///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////


var DB = new DbInteraction()
DB.findInDb({})
// DB.sendToDb({data:'Hello!'})



///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////




module.exports = {
  DbInteraction: DbInteraction,
};