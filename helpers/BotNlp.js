const vk = require('vk-easy');
require('dotenv').load();
const natural = require('natural');
const ndarray = require('ndarray');
const ops = require('ndarray-ops');
const axios = require('axios');




///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////


var BotNlp = class BotNlp {
  constructor() {
    this.name = "Natural Language Processing";
  }



  useText(arr) {
    var res = {}
    var tokenizer = new natural.AggressiveTokenizerRu();
    arr.map((item, i) => {
      var words = tokenizer.tokenize(arr[i].text)
      // console.log(words);
      var isLost = 0
      var isFound = 0
      words.map((item, i) => {
        if (same(item, 'пропал') > 0.8 || same(item, 'потерялся') > 0.8) {
          isLost++
        } else if (same(item, 'найден') > 0.8 || same(item, 'найдена') > 0.8) {
          isFound++
        }
      })
      //
      var status = 'пропал'
      if (isFound > isLost) {
        status = 'найден'
      } else if (isFound === isLost) {
        status = 'оффтоп (или дома)'
      }
      if (!res[status]) {
        res[status] = []
      }
      res[status].push(item)
    })
    return res
  }

}

///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////





module.exports = {
  BotNlp: BotNlp,
};