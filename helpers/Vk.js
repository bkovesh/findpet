const vk = require('vk-easy');
require('dotenv').load();
const natural = require('natural');
const ndarray = require('ndarray');
const ops = require('ndarray-ops');
const axios = require('axios');



///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////


var Vk = class Vk {
  constructor() {
    this.name = "Vk interaction";
    this.BOT_TOKEN = process.env.BOT_TOKEN;
    this.APP_TOKEN = process.env.APP_TOKEN;
  }



  async getWall() {
    var i = 0
    var count = await this.getWallPart(1, 1)
    console.log(count)
    count = count.count / 100

    function timeout(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    var res = []
    while (i < 1) {
      await timeout(335); // 1000ms / 3 + 2
      res.push(await this.getWallPart(100, 100 * i))
      i++
    }
    console.log(res)

    //sendToDb(res)
  }




  async getWallPart(count, offset) {
    var res = await vk('wall.get', {
      access_token: this.APP_TOKEN,
      domain: 'yapoteryalsyaspb',
      count: count,
      offset: offset
    }, true)
    //console.log(res)
    return res.response
  }



  getWallAndFindText() {
    vk('wall.get', {
      access_token: BOT_TOKEN,
      domain: 'yapoteryalsyaspb',
      count: 100,
    }, true).then(res => {
      var NLP = new NLP()
      console.log(NLP.useText(res.response.items));
    });
  }



}



///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////




module.exports = {
  Vk: Vk,
};