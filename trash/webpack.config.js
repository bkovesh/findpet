const path = require('path');

module.exports = {
  entry: './BotServer.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: './build',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  node: {
    net: 'empty',
    tls: 'empty',
    fs: 'empty',
    dns: 'empty'
  }
};